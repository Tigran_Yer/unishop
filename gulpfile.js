const gulp = require('gulp');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const autoprefixerOptions = {browsers: ['last 2 versions', '> 5%', 'Firefox ESR']};
const browserSync = require('browser-sync');
const reload = browserSync.reload;

gulp.task('styles', () => {
    gulp.src('app/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
});

gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    });
    gulp.watch("app/scss/**/*.scss", ['styles']);
    gulp.watch("app/*.html").on('change', reload);
});

gulp.task('default', ['serve'], () => {});

